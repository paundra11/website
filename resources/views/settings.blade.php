@extends ('layout.main')

@section ('content')
    <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a></li>
              <li class="breadcrumb-item active">Tambah User v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
    <div class="container-fluid">
      <form action="{{ route('settings.update', 1) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Settings</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form>
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Logo Website</label>
                    <input type="file" class="form-control" id="exampleInputEmail1" name="logo">
                    @if ($settings->logo)
                    <img src="{{ asset('storage/' . $settings->logo) }}" alt="Current Photo" class="mt-2" style="max-width: 200px;">
                    @endif
                    @error('logo')
                    <div class="alert alert-danger mt-2">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Watermark Website</label>
                    <input type="text" class="form-control" name="watermark" id="examopleInputEmail1" value="{{ $settings->watermark }}">
                    @error('watermark')
                    <div class="alert alert-danger mt-2">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
          </div>
      </form>
  </section>

</div>
@endsection