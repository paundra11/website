<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/user', [App\Http\Controllers\HomeController::class, 'index'])->name('index');
    Route::get('/create', [App\Http\Controllers\HomeController::class, 'create'])->name('user.create');
    Route::get('/edit/{id}', [App\Http\Controllers\HomeController::class, 'edit'])->name('user.edit');
    Route::delete('/delete/{id}', [App\Http\Controllers\HomeController::class, 'delete'])->name('user.delete');
    Route::put('/update/{id}', [App\Http\Controllers\HomeController::class, 'update'])->name('user.update');
    Route::post('/store', [App\Http\Controllers\HomeController::class, 'store'])->name('user.store');

    Route::get('settings/{id}/edit', [App\Http\Controllers\SettingsController::class, 'settings_edit'])->name('settings.edit');
    Route::post('settings/{id}', [App\Http\Controllers\SettingsController::class, 'settings_update'])->name('settings.update');

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
