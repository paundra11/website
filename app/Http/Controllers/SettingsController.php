<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Settings;

class SettingsController extends Controller
{
    public function settings_edit($id)
{
    $settings = Settings::findOrFail($id);
    return view('settings', compact('settings'));
}

    public function settings_update(Request $request)
    {
        $request->validate([
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'watermark' => 'string|max:255',
        ]);
    
        $settings = Settings::findOrFail(1);
    
        if ($request->hasFile('logo')) {
            $logoPath = $request->file('logo')->store('logos', 'public');
            $settings->logo = $logoPath;
        }
    
        if ($request->has('watermark')) {
            $settings->watermark = $request->input('watermark');
        }
    
        $settings->save();
    
        return redirect()->route('settings.edit', 1)->with('success', 'Data settings berhasil diupdate');
    }
     //
}
