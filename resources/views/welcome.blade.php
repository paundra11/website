<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Perpustakaan SMKN 1 Jakarta</title>
    <link rel="icon" href="{{ asset('storage/'.$settings->logo) }}" type="image/x-icon">
    <!-- FontAwesome  -->
    <link rel="stylesheet" href="{{ asset('assets/fontawesome-free-6.3.0-web/css/all.min.css') }}">
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="{{ asset('assets/bootstrap-5.0.2-dist/css/bootstrap.min.css') }}">


    <style>
        * {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
            font-family: 'poppins';
        }

        .hero {
            position: relative;
            width: 100%;
            height: 100vh;
            padding: 0 5%;
            display: flex;
            align-items: center;
            justify-content: center;
            background-image: linear-gradient(rgba(12, 3, 51, 0.3), rgba(12, 3, 51, 0.3));
        }

        .content h1 {
            font-size: 15vh;
            color: #fff;
            font-weight: 600;
            transition: 0.5s;
        }

        .content h1:hover {
            -webkit-text-stroke: 2px #fff;
            color: transparent;
        }

        .hero .content a {
            text-decoration: none;
            display: inline-block;
            font-size: 24px;
            color: #fff;
            border: 2px solid #fff;
            padding: 14px 70px;
            border-radius: 50px;
            margin-top: 20px;
        }

        .back-video {
            position: absolute;
            z-index: -1;
            right: 0;
            bottom: 0;
        }

        @media (min-aspect-ratio: 16/9) {
            .back-video {
                width: 100%;
                height: auto;
            }
        }

        @media (max-aspect-ratio: 16/9) {
            .back-video {
                width: auto;
                height: 100%;
            }
        }

        .nav-item {
            transition: all 0.5s;
        }

        .nav-item :hover {
            border-bottom: 3px solid rgb(4, 245, 137);
            font-weight: bold;
            width: max-content;
        }

        .lingkaran {
            display: inline-block;
            border-radius: 50%;
            width: 150px;
            margin: auto;
            height: 150px;
            color: #fff;
            background-color: #537fe7;
        }

        .lingkaran i {
            position: relative;
            top: 30px;
        }

        .crop-img {
            object-fit: cover;
        }

        .tim img {
            width: 250px;
            border: 10px solid #cccc;
        }

        .social {
            display: inline-block;
            width: 40px;
            height: 40px;
            color: #fff;
            border-radius: 50%;
            background-color: #3333;
            line-height: 40px;
        }

        .client img {
            height: auto;
            max-height: 100px;
        }

        .kontak {
            background: url("assets/image/bgcontact.png") no-repeat center center;
            background-size: cover;
            padding-top: 20%;
            padding-bottom: 20%;
            color: #fff;
        }

        @media (max-width :992px) {
            .content h1 {
                font-size: 10vh
            }

            .bg-perpus {
                margin: auto;
            }
        }

        @media (max-width :600px) {
            .content h1 {
                font-size: 5vh
            }

            .bg-perpus {
                margin: auto;
            }
        }
    </style>

</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-success shadow-lg">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="assets/image/smkn1jkt.gif" alt="" width="50" height="40" />
            </a>
            <a class="navbar-brand" href="#">Perpustakaan SMKN 1 Jakarta</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse text-right" id="navbarText">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="#layanan">Layanan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#fasilitas">Fasilitas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tentang">Tentang</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#staff">Staff</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Kontak Kami</a>
                    </li>
                    <li class="nav-item">
                        @if (Route::has('login'))
                        <div class="nav-link">
                            @auth
                            <a href="{{ url('dashboard') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500" style="text-decoration: none; color: white;font-family: 'poppins'; ">Home</a>
                            @else
                            <a href="{{ route('login') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500" style="text-decoration: none; color: white;font-family: 'poppins'; margin-right: 10px;">Log in</a>

                            @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500" style="text-decoration: none; color: white;font-family: 'poppins';">Register</a>
                            @endif
                            @endauth
                        </div>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- !Navbar -->

    <!-- Banner -->

    <section class="hero">

        <video src="assets/smkn1.webm" autoplay loop muted plays-inline class="back-video"></video>

        <div class="content text-center">
            <h1 class=" fw-bold ">Perpustakaan SMKN 1 Jakarta</h1>
            <a href="#">Explore</a>
        </div>
    </section>

    <!-- !Banner -->

    <!-- Layanan -->
    <div class="container-fluid layanan pt-5 pb-5 ">
        <div class="container text-center ">
            <h2 class="display-3 " id="layanan">Layanan</h2>
            <p>Di Perpustakaan SMKN 1 Jakarta, tersedia layanan layanan yang membantu kenyamanan para pengunjung, seperti :</p>
            <div class="row pt-4 ">
                <div class="col-md-4 ">
                    <span class="lingkaran "><i class="fa-solid fa-book fa-5x "></i></span>

                    <h3 class="mt-3 ">Library</h3>
                    <p>Terdapat banyak buku lengkap untuk jurusan maupun diluar kejuruan</p>
                </div>
                <div class="col-md-4 ">
                    <span class="lingkaran "><i class="fa-solid fa-desktop fa-5x "></i></span>

                    <h3 class="mt-3 ">PC</h3>
                    <p>Tersedia PC untuk keperluan para pengunjung untuk berselancar di internet</p>
                </div>
                <div class="col-md-4 ">
                    <span class="lingkaran "><i class="fa-solid fa-wifi fa-5x "></i></span>

                    <h3 class="mt-3 ">WIFI</h3>
                    <p>Ada jaringan WIFI gratis untuk para pengunjung perpustakaan</p>
                </div>
            </div>
        </div>
    </div>
    <!-- !Layanan -->

    <!-- Fasilitas -->
    <div class="container-fluid pt-5 pb-5 bg-light ">
        <div class="container text-center ">
            <h2 class="display-3 " id="fasilitas">Fasilitas</h2>
            <p>Di Perpustakaan SMKN 1 Jakarta, tersedia fasilitas fasilitas yang dapat membantu Kemudahan dalam kenyamanan para pengunjung, seperti :</p>
            <div class="row pt-4 gx-4 gy-4 ">
                <div class="col-md-4 ">
                    <div class="card crop-img ">
                        <img src="assets/image/perpus.jpg" class="card-img-top " width="200 " height="200 " />
                        <div class="card-body ">
                            <h5 class="card-title ">Scanner Kartu Pelajar</h5>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="card crop-img ">
                        <img src="assets/image/perpus.jpg" class="card-img-top " width="200 " height="200 " />
                        <div class="card-body ">
                            <h5 class="card-title ">PC </h5>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="card ">
                        <img src="assets/image/perpus.jpg" class="card-img-top " width="200 " height="200 " />
                        <div class="card-body ">
                            <h5 class="card-title ">Ruang Private Membaca </h5>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="card ">
                        <img src="assets/image/perpus.jpg" class="card-img-top " width="200" height="200" />
                        <div class="card-body ">
                            <h5 class="card-title">Loker</h5>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="card ">
                        <img src="assets/image/perpus.jpg" class="card-img-top " width="200 " height="200 " />
                        <div class="card-body ">
                            <h5 class="card-title ">Rak buku</h5>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="card ">
                        <img src="assets/image/perpus.jpg" class="card-img-top " width="200 " height="200 " />
                        <div class="card-body ">
                            <h5 class="card-title ">Sofa </h5>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- !Fasilitas -->

    <!-- Tentang -->
    <div class="container-fluid pt-5 pb-5 ">
        <div class="container ">
            <h2 class="display-3 text-center " id="tentang">Tentang</h2>
            <p class="text-center ">Perpustakaan SMKN 1 Jakarta</p>
            <div class="clearfix pt-5">
                <img src="assets/image/smkn1foto.jpg" alt=" " class="col-md-6 float-md-end mb-3 crop-img bg-perpus" width="300" height="300" />
                <p style="text-align: justify;">Pada dasarnya, Perpustakaan adalah sebuah tempat di mana seseorang bisa menemukan dan meminjam berbagai jenis buku dan media lainnya, seperti majalah, jurnal, koran, dan DVD. Perpustakaan umumnya memiliki koleksi buku yang luas dan beragam, dari berbagai genre dan topik seperti fiksi, non-fiksi, sejarah, sains, teknologi, dan banyak lagi.</p>
                <p style="text-align: justify;">Selain sebagai tempat untuk meminjam buku, perpustakaan SMKN 1 Jakarta juga berfungsi sebagai pusat informasi dan sumber daya untuk pendidikan dan penelitian. Di dalam perpustakaan, terdapat meja dan kursi untuk membaca dan belajar, serta komputer dan akses internet untuk mencari informasi online.</p>
                <p style="text-align: justify;">Perpustakaan SMKN 1 Jakarta merupakan sumber daya yang sangat berharga bagi Warga Sekolah. Dengan memiliki akses ke perpustakaan, siapapun dapat meningkatkan pengetahuan dan pemahaman tentang dunia, meningkatkan keterampilan membaca, menemukan inspirasi, serta mengeksplorasi minat dan bakat mereka.</p>
            </div>
        </div>
    </div>
    <!-- !Tentang -->

    <!-- Staff -->
    <div class="container-fluid pt-5 pb-5 bg-light ">
        <div class="container text-center ">
            <h2 class="display-3 " id="staff">Staff</h2>
            <p>Staff Sekolah</p>
            <div class="row pt-4 gx-4 gy-4 ">
                <div class="col-md-4 text-center tim ">
                    <img src="assets/image/guru.png" class="rounded-circle " alt="... " />
                    <h4>Awalusilman, M.PD.</h4>
                    <p>Kepala Sekolah</p>
                    <p>
                        <a href="#" class="social "><i class="fa-brands fa-twitter "></i></a>
                        <a href="#" class="social "><i class="fa-brands fa-facebook "></i></a>
                        <a href="#" class="social "><i class="fa-brands fa-instagram "></i></a>
                    </p>
                </div>
                <div class="col-md-4 text-center tim ">
                    <img src="assets/image/guru.png" class="rounded-circle " alt="... " />
                    <h4>Ismi, M.Pd</h4>
                    <p>Kepala Perpustakaan</p>
                    <p>
                        <a href="#" class="social "><i class="fa-brands fa-twitter "></i></a>
                        <a href="#" class="social "><i class="fa-brands fa-facebook "></i></a>
                        <a href="#" class="social "><i class="fa-brands fa-instagram "></i></a>
                    </p>
                </div>
                <div class="col-md-4 text-center tim ">
                    <img src="assets/image/guru.png" class="rounded-circle " alt="... " />
                    <h4>Amrul Khairullah </h4>
                    <p>Guru PKK</p>
                    <p>
                        <a href="#" class="social "><i class="fa-brands fa-twitter "></i></a>
                        <a href="#" class="social "><i class="fa-brands fa-facebook "></i></a>
                        <a href="#" class="social "><i class="fa-brands fa-instagram "></i></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- !Staff -->

    <!-- Client -->
    <div class="container-fluid pt-5 pb-5 client ">
        <div class="container text-center ">
            <div class="row pt-4 gx-4 gy-4 ">
                <div class="col ">
                    <img src="assets/image/RPL.jpeg" alt=" " />
                </div>
                <div class="col ">
                    <img src="assets/image/tut.webp" alt=" " />
                </div>
                <div class="col ">
                    <img src="assets/image/smkn1jkt.gif" alt=" " />
                </div>
            </div>
        </div>
    </div>
    <!-- !Client -->

    <!-- Contact -->
    <div class="container-fluid pt-5 pb-5 kontak ">
        <div class="container ">
            <h2 class="display-3 text-center " id="contact">Kontak Kami</h2>
            <p class="text-center ">Saran, komentar dan penilaian yang dapat membantu kami </p>
            <div class="alert alert-success alert-dismissible fade show d-none" role="alert">
                <strong>Terimakasih ! </strong> Saran anda adalah Motivasi bagi kami
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <form action=" " name="myForm">
                <div class="row pb-3 ">
                    <div class="col-md-6 ">
                        <input type="text " class="form-control form-control-lg mb-3 " placeholder="Nama " name="nama" id="nama " />
                        <input type="email " class="form-control form-control-lg mb-3 " placeholder="Email " name="email" id="email " />
                        <input type="number " class="form-control form-control-lg " placeholder="No Handphone " name="nohp" id="nohp " />
                    </div>
                    <div class="col-md-6 ">
                        <textarea placeholder="Komentar " id=" " class="form-control form-control-lg mt-2 " cols="30 " rows="5 " name="komentar" id="komentar "></textarea>
                    </div>
                    <div class="col-md-3 mt-4 ">
                        <button type="submit " id="" class="form-control btn btn-success btn-kirim">Kirim</button>
                        <button class="btn btn-success btn-loading d-none" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Loading...
                        </button>
                    </div>
                </div>
        </div>
        </form>
    </div>
    <!-- !Contact -->
    <div class="container text-center pt-5 pb-5 ">All Right Reserved {{$settings->watermark}} &copy; 2024</div>
    <!-- SpreadSheet -->
    <!-- Boostraps JS -->
    <script src="{{ asset('assets/bootstrap-5.0.2-dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/bootstrap-5.0.2-dist/js/bootstrap.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/Poppins-Regular.ttf') }}"></script>

</body>

</html>