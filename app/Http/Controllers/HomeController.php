<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function dashboard (){
        return view('dashboard');
    }

    public function index()
    {
        $data = User::get(); 

        return view('index', compact('data')); 
    }

    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
{
    $validator = Validator::make($request->all(), [
        'email' => 'required|email',
        'nama' => 'required',
        'password' => 'required',
    ]);

    if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

    $data['email'] = $request->email;
    $data['name'] = $request->nama;
    $data['password'] = Hash::make($request->password);

    User::create($data);
    return redirect()->route('index');
}


    public function edit (request $request, $id){
        $data = User::find($id);
        return view('edit',compact ('data'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email',
            'nama'     => 'required',
            'password' => 'nullable',
        ]);
    
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
    
        $user = User::find($id);
    
        if (!$user) {
            // Handle the case where the user with the given $id is not found.
            return redirect()->route('admin.index')->with('error', 'User not found');
        }

        if ($request->filled('password')) {
            // Update the password only if a new password is provided
            $data['password'] = Hash::make($request->password);
        }
    
        $find = User::find($id);

        $data['email'] = $request->email;
        $data['name'] = $request->nama;

        $find->update($data);
    
        return redirect()->route('index')->with('success', 'User updated successfully');
    }
    

    public function delete(Request $request,$id){
        $data = User::find($id);
        if($data){
            $data->delete();
        }
        return redirect()->route('index');
    }
}
